# set fileencoding: utf-8
import shutil
import tempfile
import unittest
from argparse import Namespace
from os import path

from antismash.specific_modules.nrpspks import parsers


class TestNRPSParser(unittest.TestCase):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.options = Namespace()
        self.pksnrpsvars = Namespace()

        self.options.raw_predictions_outputfolder = self.tmpdir
        self.options.record_idx = 1

        self.pksnrpsvars.nrpsnames = ['demO_A1', 'demO_A2']

        with open(path.join(self.tmpdir, 'ctg1_ind.res.tsv'), 'w') as fh:
            fh.write("demO_A1\tprediCAT_MP\tQ939Z1_A1\n")
            fh.write("demO_A1\tprediCAT_SNN\tno_force_needed\t1.234567\n")
            fh.write("demO_A1\tASM\tleu\n")
            fh.write("demO_A1\tSVM\tleu\n")
            fh.write("demO_A1\tpHMM\tleu\n")
            fh.write("demO_A2\tprediCAT_MP\tQ939Z1_A2\n")
            fh.write("demO_A2\tprediCAT_SNN\tno_force_needed\t2.345678\n")
            fh.write("demO_A2\tASM\tbht|tyr\n")
            fh.write("demO_A2\tSVM\tbht\n")
            fh.write("demO_A2\tpHMM\tbht\n")

        with open(path.join(self.tmpdir, 'ctg1_pid.res.tsv'), 'w') as fh:
            fh.write("demO_A1\t42.00\n")
            fh.write("demO_A2\t23.00\n")


        with open(path.join(self.tmpdir, 'ctg1_sandpuma.tsv'), 'w') as fh:
            fh.write("demO_A1\tSANDPUMA\tno_call\n")
            fh.write("demO_A2\tSANDPUMA\tbht\n")

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_parser(self):
        expected_allmethods = {
            'demO_A1': {
                'prediCAT_MP': 'Q939Z1_A1',
                'prediCAT_SNN': 'no_force_needed',
                'ASM': 'leu',
                'SVM': 'leu',
                'pHMM': 'leu',
            },
            'demO_A2': {
                'prediCAT_MP': 'Q939Z1_A2',
                'prediCAT_SNN': 'no_force_needed',
                'ASM': 'bht|tyr',
                'SVM': 'bht',
                'pHMM': 'bht',
            }
        }

        expected_snn = {
            'demO_A1': '1.234567',
            'demO_A2': '2.345678'
        }

        expected_pid = {
            'demO_A1': '42.00',
            'demO_A2': '23.00'
        }

        expected_res = {
            'demO_A1': 'no_call',
            'demO_A2': 'bht'
        }

        parsers.parse_nrps_preds(self.options, self.pksnrpsvars)
        assert 'sandpuma_allmethods' in self.pksnrpsvars
        self.assertEqual(expected_allmethods, self.pksnrpsvars.sandpuma_allmethods)
        assert 'sandpuma_snn' in self.pksnrpsvars
        self.assertEqual(expected_snn, self.pksnrpsvars.sandpuma_snn)
        assert 'sandpuma_pid' in self.pksnrpsvars
        self.assertEqual(expected_pid, self.pksnrpsvars.sandpuma_pid)
        assert 'sandpuma_res' in self.pksnrpsvars
        self.assertEqual(expected_res, self.pksnrpsvars.sandpuma_res)
